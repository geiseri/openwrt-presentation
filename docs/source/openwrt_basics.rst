About OpenWRT
=============

.. ifslides::

    * Started with Linksys GPL code
    * Community supported
    * First release in 2006
    * Over 300 types of devices

      * https://wiki.openwrt.org/toh/start

.. ifnotslides::

    OpenWRT is a great example of how a company who complies with the
    OSS licenses can spawn a community.  OpenWRT started as a fork of
    the GPL components that Linksys released for their WRTG-54g router
    back in 2003.

    The community took this code and extended it with a custom user
    interface and tools that took the product far beyond what the
    engineers at Linksys first imagined.

    Since the first release back in 2006 the number of devices supported
    has exploded to over 300 types of consumer and commercial grade
    routers.

Configuring OpenWRT
===================

.. ifslides::

    * UCI - command line
    * LuCI - HTML5

.. ifnotslides::

    OpenWRT has two main configuration options UCI a command line
    application that is accessible with SSH or the console and LuCI
    the web interface.

UCI
---

.. image:: images/uci.png
    :alt: luci command line interface

.. code-block:: ini
    :linenos:

    config 'wifi-device' 'radio0'
        option 'type' 'mac80211'
        option 'hwmode' '11g'
        option 'htmode' 'HT20'
        option 'country' '00'
        option 'channel' '11'

.. ifnotslides::

    UCI gives a structured view of the key/value pairs of each
    configuration file.  The tool can read and write values to a
    temporary instance of each configuration file and then commit to
    the physical file when all of the edits are complete.  Since the UCI
    tool is a command line utility it makes it easier to script things
    for dynamic operations.


LuCI
----

.. image:: images/luci.png
    :alt: luci web interface

.. ifnotslides::

    LuCI has a more visual approach where properties of a configuration
    are edited in a HTML form and then applied once editing is complete.
    Unlike the UCI tool the HTML interface gives a richer amount of
    feedback to the user at the expense of automation.
