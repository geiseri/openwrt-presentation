Going Further
=============

.. ifslides::

    * Captive WiFI portal
    * TOR gateway
    * PTP VPN tunnel
    * DNLA media streamer
    * See https://wiki.openwrt.org/doc/start for more!

.. ifnotslides::

    OpenWRT includes over 3000 packages that can be added to an image. These
    packages include everything from captive portals to proxies to VPNs.  There are
    even applications beyond just a router like a DNLA media streamer or a samba
    file share.  While not all of these applications have documentation many of them
    are documented on the OpenWRT wiki.  You can also search github for many other
    people's contributions.
