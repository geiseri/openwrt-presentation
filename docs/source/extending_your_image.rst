Extending your image
====================

.. ifnotslides::

    Once an image has been built it can be installed on the device as is
    and configure it during run time.  Settings are persisted between reboots
    and optionally between firmware updates.  In some cases though it is
    desirable to have specific settings as defaults on the image. OpenWRT
    allows for the adding of files in the base image.  The advantage of
    doing this is that these settings will survive a factory reset as well
    as come up correctly on first boot.

Custom default settings
-----------------------

.. code-block:: console

    $ mkdir -p $BASE_DIR/files/etc/config
    $ $EDITOR $BASE_DIR/files/etc/config/network

.. ifnotslides::

    To add files is very easy.  Under the source checkout a ``files`` directory
    would be created. Then the directory structure under that directory will be
    copied into the final image.

    It is recommended that settings are tested on the device first and then
    copied into the project.  Otherwise there could end up with a bad default
    configuration that may not even allow the image to boot.

Common files to include
-----------------------

.. ifslides::

    * System - ``/etc/config/system``
    * Network - ``/etc/config/network``
    * Wireless - ``/etc/config/wireless``

.. ifnotslides::

    Most common configuration files that the OpenWRT configuration system uses
    are in a single directory.  In the applications for this presentation the key
    ones are system, network, and wireless.  In most cases if these files are not
    included in the image they are generated with sensible defaults on the first
    boot.  This is the case for configurations as firewall and DHCP settings.

System Configuration File Structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: ini
    :linenos:

    config 'system'
            option '<key>' '<value>'

    config 'timeserver' 'ntp'
        list 'server' '<server1>'
        list 'server' '<server2>'
        option enable_server 0

.. ifnotslides::

    The system configuration file is in the OpenWRT UCI format.  This format
    is acceptable at run time via the ``uci`` command or the web interface.  In
    the case of this example the file will be embedded in the image at build time.

    The main components covered here are the system and the time server sections.

Common System Options
~~~~~~~~~~~~~~~~~~~~~

.. ifslides::

    * System
        * hostname
        * timezone
    * Time server
        * server
        * enable_server

.. ifnotslides::

    Commonly used options in the system configuration are the hostname and timezone.
    In the timeserver section there are options to set and enable NTP servers.

    ``system.hostname``
        Hostname displayed by the router.  In cases that the router is providing
        DHCP and DNS this name will be used for the gateway.

    ``system.timezone``
        The timezone string that will be used for local date-time displays on
        the router.  This code is specific to OpenWRT and the full list of codes
        can be found at: https://wiki.openwrt.org/doc/uci/system#time_zones

    ``timeserver.server``
        This is a list of NTP servers to query for the current time. In the case
        that there are no servers defined then the builtin NTP server is not
        enabled.

    ``timeserver.enable_server``
        This is an optional setting that will explicitly enable or disable the
        NTP server on the router.


Network Configuration File Structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: ini
    :linenos:

    config 'interface' '<name>'
            option '<key>' '<value>'

.. ifnotslides::

   The structure of the network configuration file should seem familiar to those
   who have edited the Debian network ``interfaces`` file.  It has a section
   where you can define the interface and then a set of options for that interface.

   There is another configuration type called ``switch`` that can be used for embedded
   devices that support hardware switches.  This can be used to set up VLANs
   specific to parts of the switch if needed.

   For the case of our examples we will be focused on discrete network interfaces.

Common Network Interface Options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. ifslides::

    * proto
    * type
    * ifname
    * enabled
    * ipaddr
    * netmask
    * gateway
    * dns

.. ifnotslides::

    The main option for the network interface configuration is the protocol.  This paper
    will be focusing on static and DHCP configurations.  There is in fact support
    for 3g modems, PPP, PPPoE and many other protocols.

    ``proto``
        Protocol used to define the interface it is usually static, or DHCP for
        most cases.

    ``type``
        This allows for the changing of the interface type to ``bridge``
        Normally this value is left undefined.

    ``ifname``
        The name of the physical interface that is configured by this section.
        In cases that the type defined is ``bridge`` then this value can have multiple
        interfaces listed.

    ``enabled``
        This can allow the interface to explicitly be enabled or disabled by
        default.  This is helpful if the interface needs extra configuration at run time
        before it is ready to be used.

    ``ipaddr``
        The IP address of the interface. Static configuration protocol only.

    ``netmask``
        The netmask given to the interface. Static configuration protocol only.

    ``gateway``
        The default gateway for the interface.  Static configuration protocol only.

    ``dns``
        A list of DNS servers that are used for this interface.  This is an
        optional setting for the static configuration protocol.


Wireless Configuration File Structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: ini
    :linenos:

    interface 'wifi-device' '<name>'
        option '<key>' '<value>'

    config wifi-iface
        option 'device' '<wifi-device.name>'
        options 'network' '<network name>'
        option '<key>' '<value>'


.. ifnotslides::

    The wireless configuration file structure is broken into two distinct parts.

    The first part is the "wifi-device" description.  This is the definition
    for the radio that will be used.  The options are a list of key and value pairs.

    The second part is the actual definition of the wireless interface.  This would
    map to the network device such as ``wlan0`` or, ``wwan``, etc.  There are two
    mandatory options that need to be present. The first one is the device.  This
    maps to the name used for the ``wifi-device`` in the first section.  The
    second option is the ``network``.  The reason that the sections are separate
    is because you can have multiple interfaces per radio.  The interfaces each
    map to a ``network``.  Usually devices are mapped to networks in the
    ``/etc/config/network`` configuration file.  Since the interface names are
    not deterministic the network is set here and maps back to the network name
    in the ``/etc/config/network`` configuration file.


Common Wireless Interface Options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. ifslides::

    * type
    * hwmode
    * ifname
    * disabled
    * channel
    * country

.. ifnotslides::

    ``type``
        This is the driver of wireless interface.  Usually it is ``mac80211``
        for modern wireless cards.  For embedded platforms they may need special drivers
        such as ``ath5k`` or ``broadcom``.  Most of the time this value can be
        auto-detected.

    ``ifname``
        This is the name of the wireless interface.  In most cases this value is
        automatically determined, but you can set it when you have hard coded settings
        that need a deterministic wireless card name.

    ``hwmode``
        This is the protocol that the wireless card will operate on.  The three
        acceptable options are ``11b``, ``11g``, and ``11a``.  In practice ``11g``
        enabled all 2.4Ghz protocols and ``11a`` enabled all supported 5Ghz protocols.

    ``disabled``
        This setting toggles the on and off states of the radio. Most cases this
        can be used to disable the radio in cases that there needs to be further
        configuration at run time to make it functional.

    ``channel``
        This is the setting that selects the channel that the wireless card will
        function on.  In most cases you can leave this value as ``auto`` so it will
        select the minimum available channel.

    ``country``
        This is the country code that will dictate what channels and
        transmission powers are available to the radio.  If this is empty it will use
        the card's default but for best performance this value should be set.

Common Wireless Configuration Options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. ifslides::

    * device
    * mode
    * ssid
    * encryption
    * key
    * network
    * disabled
    * hidden
    * isolate

.. ifnotslides::

    ``device``
        This is the name of the device.  If there was a ``ifname`` defined in
        the interface options then it would be here.  In most cases if there is only one
        wireless card it would be ``radio0``

    ``mode``
        This selects the mode of the wireless interface. In most cases it
        would be ``ap``, but if you want the card to operate as a client ``sta`` would
        be used.

    ``ssid``
        This sets the SSID that will be used for the wireless network.  In cases
        of the mode being ``ap`` this would be the SSID broadcast to clients.  In the
        ``sta`` mode this would be the SSID to attach to.

    ``encryption``
        This is the encryption type the wireless network will use. In cases of
        the mode being ``ap`` this would be the encryption type clients would need to
        connect with.  In the ``sta`` mode it would be the encryption type the wireless
        card will connect to the SSID.  In most cases ``psk`` for WPA personal passkey,
        or ``psk2`` to use WPA2 personal passkey. If there is no password then the value
        of ``open`` can be used.

    ``key``
        This setting is the WPA key associated with the network. In cases of the
        mode being ``ap`` then this would be the passkey clients would use to connect to
        the SSID.  In the ``sta`` mode this would be the passkey used to connect to the
        SSID.

    ``network``
        This is the name of the network to associate the wireless network with.
        Wireless configurations do not have a deterministic interface name so it is
        important to associate the network here.  This would then be excluded from the
        ``network`` configuration.

    ``disabled``
        This setting toggles the on and off states of the radio. Most cases this
        can be used to disable the radio in cases that there needs to be further
        configuration at run time to make it functional.

    ``hidden``
        This setting is only valid in ``ap`` mode.  When set to ``1`` it will
        disable the broadcast of the SSID.

    ``isolate``
        This setting is only valid in ``ap`` mode.  When set to ``1`` it will
        make it so wireless clients cannot communicate with each other.
