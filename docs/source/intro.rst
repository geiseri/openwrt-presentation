.. ifslides::

    Agenda
    ======

    #. Why wouldn't you do this
    #. Why would you do this
    #. About OpenWRT
    #. Configuring OpenWRT
    #. Building your first image
    #. Extending your image
    #. Fun applications!

Why wouldn't you do this
========================

.. ifslides::

    * Will not save money
    * Not a lot of time on your hands
    * Not comfortable with build tools

.. ifnotslides::

    There are plenty of reasons why building your own router is a bad
    idea.  The most important one is that you will not save money.  By
    the time you get the parts it would be cheaper just to buy a mid-range
    router.  The other factor is time.  Tweaking builds, reflashing and
    subsequent debugging can be a real time sink for even the most
    experienced geek.  That also brings up the last reason you wouldn't want
    to do this. If you are not comfortable with build tools this process
    can be a real nightmare.

Why would you do this
=====================

.. ifslides::

    * Learn about embedded systems
    * Hackable networking playground
    * Because you can

.. ifnotslides::

    If those reasons do not scare you off then there are some great
    opportunities that await you. The coolest thing about OpenWRT is that
    it can give an introduction to working with and developing for
    embedded systems. Once you have a working system you also have a
    flexible playground for exploring the wonderful world of networking.
    Lastly like all great engineering challenges we do them because we
    can!
