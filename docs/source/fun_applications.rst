Fun applications!
=================

.. ifnotslides::

    Now that the basics of building and configuring an image it is time to apply
    it to some projects. The first application will be a "switch" that can be used
    for network debugging.  The second application is a private access point that
    will secure an otherwise insecure wireless network.

.. include:: debug_switch.rst
.. include:: private_access_point.rst
