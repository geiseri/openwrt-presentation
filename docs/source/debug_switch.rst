Debug Switch
------------

.. ifslides::

    * Uses 8 port x86 board
    * Bridges all 8 ports
    * View switch traffic
    * Gather switch metrics

.. image:: images/switch_board.jpg
    :alt: jetway 8 port board

.. ifnotslides::

    One cool thing about OpenWRT is that it not only makes configuring hard
    network things easy, it is also extensive enough to include advanced debugging
    tools.  This application is a software based switch.  It uses a Jetway x86 based
    board with 8 built in Ethernet ports.  The ports are bridged together so that as
    far as devices connected to the Ethernet are concerned it is an ordinary switch.

    From this platform captures can be performed to visualize network traffic at
    a packet level.  Network load metrics can also be viweed from the LuCI web
    interface.


Image Configuration
~~~~~~~~~~~~~~~~~~~

.. code-block:: kconfig
    :linenos:

    CONFIG_PACKAGE_luci-app-vnstat=y
    CONFIG_PACKAGE_tcpdump=y
    CONFIG_PACKAGE_kmod-igb=y

.. ifnotslides::

    Most of the configuration that is needed for this application is already
    included in the base configuration template from earlier. The key things to add
    are the debugging and visualization tools for making the switch useful.

    Line 1 adds a web interface for the utility ``vnstat``.  This tool will help
    an administrator to see traffic statistics for the switch.

    The second line adds the popular packet dumping tool ``tcpdump``.

    The last line is needed to add support for the 8 port Ethernet controller.

Device Configuration
~~~~~~~~~~~~~~~~~~~~

.. ifslides::

    * ``/etc/config/network``

.. code-block:: ini
    :linenos:

    config interface lan
    option type     'bridge'
    option ifname   'eth0 eth1 eth2 eth3 eth4 eth5 eth6 eth7 eth8'
    option proto    'dhcp'

.. ifnotslides::

    The actual configuration is quite minimal.  Only network file needs to be
    changed.  In this case to emulate a switch a bridge is created of all of the
    physical interfaces.  While not as efficient as a real switch the x86 CPU is more
    than capable of managing the traffic.

Putting it Together
~~~~~~~~~~~~~~~~~~~

.. ifslides::

    * remote tcpdump from switch

.. code-block:: console

    $ ssh root@HOST tcpdump -U -s0 -w - 'not port 22'  wireshark -k -i -

.. ifnotslides::

    Being able to capture remote traffic at a switch level is a big help when dealing
    with strange network errors.  Normally in a unmanaged switch there is no insight into
    the entire network's traffic. Since the "switch" here is actually a bridge
    network interface ``tcpdump`` can be run directly on that interface.  Then
    the output can be piped over a SSH connection to a client running wireshark.
    One thing to be aware of is that port 22 traffic should be filtered to keep
    ``tcpdump`` from capturing the packets its sending to the client.  Different
    filters can be applied depending on what the user is looking for and how much
    bandwidth that should be captured.  Once the packets are in wireshark there is
    a slick user interface that will give insight into the local network.

.. ifslides::

    * view traffic statistics

.. image:: images/vnstat.png
    :alt: vnstat LuCI output

.. ifnotslides::

    One other useful feature of this "switch" is that network traffic can be
    monitored and metrics gathered.  A nice tool for OpenWRT is called ``vnstat``.
    This tool will generate graphs of various network metrics over time.  This
    can help identify top consumers of network resources or just generate
    pretty graphs to look at.
