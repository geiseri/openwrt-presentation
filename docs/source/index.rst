:data-transition-duration: 2000
:skip-help: true
:slide-numbers: true

==================================
Build your own Router with OpenWRT
==================================

**Docs:** http://openwrt-presentation.rtfd.io

**Repo:** http://bitbucket.org/geiseri/openwrt-presentation

**By:** Ian Reinhart Geiser

.. ifnotslides::

    My name is Ian Geiser and I am actually not a developer or in any way
    associated with the OpenWRT project.  I just wanted to learn more about it so I
    decided to research it for this conference. This presentation is a result of my
    adventures in learning about OpenWRT.  A static version of this document can be
    found at Read the Docs.  I have also included resources and the meta-data in my
    bitbucket repository.

    .. toctree::
        :maxdepth: 2

        intro.rst
        openwrt_basics.rst
        building_your_first_image.rst
        extending_your_image.rst
        fun_applications.rst
        going_further.rst


.. ifslides::

    .. include:: intro.rst
    .. include:: openwrt_basics.rst
    .. include:: building_your_first_image.rst
    .. include:: extending_your_image.rst
    .. include:: fun_applications.rst
    .. include:: going_further.rst

.. ifslides::

    Questions?
    ==========
