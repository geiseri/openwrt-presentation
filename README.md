# README #
This is the repository for the paper I gave at SELF 2017

Published at:  http://openwrt-presentation.rtfd.io

# Building Slides #

```
$ cd docs
$ pip install -r requirements.txt
$ make slides
```

Slides will be found in build/slides/index.html

# Build Environment #

## Create build environment ##

* Edit ``Dockerfile`` and change line 11 to match your user's UID
* Build the container

```
$ docker build -t geiseri/openwrt .
```

## Run build environment ##

```
$ cd $BUILDROOT
$ docker run --rm -ti -e TERM -v ${PWD}:/host geiseri/openwrt bash
```
