
# Create build environment

* Edit ``Dockerfile`` and change line 11 to match your user's UID
* Build the container

```
docker build -t geiseri/openwrt .
```

# Run build environment

```
cd $BUILDROOT
docker run --rm -ti -e TERM -v ${PWD}:/host geiseri/openwrt bash
```
